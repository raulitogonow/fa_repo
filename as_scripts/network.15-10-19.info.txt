* Hostname: alszla
* Run date and time: Tue Oct 15 15:45:22 -04 2019
---------------------------------------------------
Linux Distro
---------------------------------------------------
Linux kernel: Linux 4.4.0-165-generic x86_64
Distributor ID:	Ubuntu
Description:	Ubuntu 16.04.6 LTS
Release:	16.04
Codename:	xenial
---------------------------------------------------

---------------------------------------------------
---------------------------------------------------

---------------------------------------------------
---------------------------------------------------
PCI Devices
---------------------------------------------------
00:00.0 Host bridge: Intel Corporation 440FX - 82441FX PMC [Natoma] (rev 02)
	Subsystem: Red Hat, Inc. Qemu virtual machine
	Flags: bus master, fast devsel, latency 0

00:01.0 ISA bridge: Intel Corporation 82371SB PIIX3 ISA [Natoma/Triton II]
	Subsystem: Red Hat, Inc. Qemu virtual machine
	Flags: medium devsel

00:01.1 IDE interface: Intel Corporation 82371SB PIIX3 IDE [Natoma/Triton II] (prog-if 80 [ISA Compatibility mode-only controller, supports bus mastering])
	Subsystem: Red Hat, Inc. Qemu virtual machine
	Flags: bus master, medium devsel, latency 0
	[virtual] Memory at 000001f0 (32-bit, non-prefetchable) [size=8]
	[virtual] Memory at 000003f0 (type 3, non-prefetchable)
	[virtual] Memory at 00000170 (32-bit, non-prefetchable) [size=8]
	[virtual] Memory at 00000370 (type 3, non-prefetchable)
	I/O ports at c100 [size=16]
	Kernel driver in use: ata_piix
	Kernel modules: pata_acpi

00:01.2 USB controller: Intel Corporation 82371SB PIIX3 USB [Natoma/Triton II] (rev 01) (prog-if 00 [UHCI])
	Subsystem: Red Hat, Inc. QEMU Virtual Machine
	Flags: bus master, fast devsel, latency 0, IRQ 11
	I/O ports at c0c0 [size=32]
	Kernel driver in use: uhci_hcd

00:01.3 Bridge: Intel Corporation 82371AB/EB/MB PIIX4 ACPI (rev 03)
	Subsystem: Red Hat, Inc. Qemu virtual machine
	Flags: medium devsel, IRQ 9
	Kernel driver in use: piix4_smbus
	Kernel modules: i2c_piix4

00:02.0 VGA compatible controller: Cirrus Logic GD 5446 (prog-if 00 [VGA controller])
	Subsystem: Red Hat, Inc. QEMU Virtual Machine
	Flags: fast devsel
	Memory at fc000000 (32-bit, prefetchable) [size=32M]
	Memory at feb90000 (32-bit, non-prefetchable) [size=4K]
	Expansion ROM at feb80000 [disabled] [size=64K]
	Kernel driver in use: cirrus
	Kernel modules: cirrusfb, cirrus

00:03.0 Ethernet controller: Red Hat, Inc. Virtio network device
	Subsystem: Red Hat, Inc. Virtio network device
	Physical Slot: 3
	Flags: bus master, fast devsel, latency 0, IRQ 10
	I/O ports at c000 [size=64]
	Memory at feb91000 (32-bit, non-prefetchable) [size=4K]
	Memory at fe000000 (64-bit, prefetchable) [size=16K]
	Expansion ROM at feb00000 [disabled] [size=512K]
	Capabilities: [98] MSI-X: Enable+ Count=3 Masked-
	Capabilities: [84] Vendor Specific Information: VirtIO: <unknown>
	Capabilities: [70] Vendor Specific Information: VirtIO: Notify
	Capabilities: [60] Vendor Specific Information: VirtIO: DeviceCfg
	Capabilities: [50] Vendor Specific Information: VirtIO: ISR
	Capabilities: [40] Vendor Specific Information: VirtIO: CommonCfg
	Kernel driver in use: virtio-pci

00:04.0 Communication controller: Red Hat, Inc. Virtio console
	Subsystem: Red Hat, Inc. Virtio console
	Physical Slot: 4
	Flags: bus master, fast devsel, latency 0, IRQ 11
	I/O ports at c040 [size=64]
	Memory at feb92000 (32-bit, non-prefetchable) [size=4K]
	Memory at fe004000 (64-bit, prefetchable) [size=16K]
	Capabilities: [98] MSI-X: Enable+ Count=2 Masked-
	Capabilities: [84] Vendor Specific Information: VirtIO: <unknown>
	Capabilities: [70] Vendor Specific Information: VirtIO: Notify
	Capabilities: [60] Vendor Specific Information: VirtIO: DeviceCfg
	Capabilities: [50] Vendor Specific Information: VirtIO: ISR
	Capabilities: [40] Vendor Specific Information: VirtIO: CommonCfg
	Kernel driver in use: virtio-pci

00:05.0 SCSI storage controller: Red Hat, Inc. Virtio block device
	Subsystem: Red Hat, Inc. Virtio block device
	Physical Slot: 5
	Flags: bus master, fast devsel, latency 0, IRQ 10
	I/O ports at c080 [size=64]
	Memory at feb93000 (32-bit, non-prefetchable) [size=4K]
	Memory at fe008000 (64-bit, prefetchable) [size=16K]
	Capabilities: [98] MSI-X: Enable+ Count=2 Masked-
	Capabilities: [84] Vendor Specific Information: VirtIO: <unknown>
	Capabilities: [70] Vendor Specific Information: VirtIO: Notify
	Capabilities: [60] Vendor Specific Information: VirtIO: DeviceCfg
	Capabilities: [50] Vendor Specific Information: VirtIO: ISR
	Capabilities: [40] Vendor Specific Information: VirtIO: CommonCfg
	Kernel driver in use: virtio-pci

00:06.0 Unclassified device [00ff]: Red Hat, Inc. Virtio memory balloon
	Subsystem: Red Hat, Inc. Virtio memory balloon
	Physical Slot: 6
	Flags: bus master, fast devsel, latency 0, IRQ 11
	I/O ports at c0e0 [size=32]
	Memory at fe00c000 (64-bit, prefetchable) [size=16K]
	Capabilities: [84] Vendor Specific Information: VirtIO: <unknown>
	Capabilities: [70] Vendor Specific Information: VirtIO: Notify
	Capabilities: [60] Vendor Specific Information: VirtIO: DeviceCfg
	Capabilities: [50] Vendor Specific Information: VirtIO: ISR
	Capabilities: [40] Vendor Specific Information: VirtIO: CommonCfg
	Kernel driver in use: virtio-pci

---------------------------------------------------
 Output
---------------------------------------------------
---------------------------------------------------
Kernel Routing Table
---------------------------------------------------
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         10.28.130.1     0.0.0.0         UG    0      0        0 ens3
10.28.130.0     0.0.0.0         255.255.255.0   U     0      0        0 ens3
169.254.169.254 10.28.130.1     255.255.255.255 UGH   0      0        0 ens3
---------------------------------------------------
Network Card Drivers Configuration /etc/modprobe.conf
---------------------------------------------------
Error /etc/modprobe.conf file not found.
---------------------------------------------------
DNS Client /etc/resolv.conf Configuration
---------------------------------------------------
# Dynamic resolv.conf(5) file for glibc resolver(3) generated by resolvconf(8)
#     DO NOT EDIT THIS FILE BY HAND -- YOUR CHANGES WILL BE OVERWRITTEN
nameserver 10.28.143.47
search openstacklocal
---------------------------------------------------
Network Configuration File
---------------------------------------------------
Error /etc/sysconfig/network-scripts/ifcfg-eth? not found.
---------------------------------------------------
Network Aliase File
---------------------------------------------------
Error /etc/sysconfig/network-scripts/ifcfg-eth?-range? not found.
---------------------------------------------------
Network Static Routing Configuration
---------------------------------------------------
Error /etc/sysconfig/network-scripts/route-eth? not found.
---------------------------------------------------
IP4 Firewall Configuration
---------------------------------------------------
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
---------------------------------------------------
IP6 Firewall Configuration
---------------------------------------------------
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
---------------------------------------------------
Network Stats
---------------------------------------------------
Ip:
    2235457 total packets received
    14 with invalid addresses
    0 forwarded
    0 incoming packets discarded
    2224019 incoming packets delivered
    1161016 requests sent out
Icmp:
    0 ICMP messages received
    0 input ICMP message failed.
    ICMP input histogram:
    0 ICMP messages sent
    0 ICMP messages failed
    ICMP output histogram:
Tcp:
    5629 active connections openings
    34 passive connection openings
    1991 failed connection attempts
    1455 connection resets received
    7 connections established
    2189600 segments received
    3919871 segments send out
    1517 segments retransmited
    6 bad segments received.
    4281 resets sent
Udp:
    33586 packets received
    0 packets to unknown port received.
    396 packet receive errors
    19803 packets sent
    RcvbufErrors: 396
    IgnoredMulti: 4135
UdpLite:
TcpExt:
    495 TCP sockets finished time wait in fast timer
    10 packets rejects in established connections because of timestamp
    18423 delayed acks sent
    39 delayed acks further delayed because of locked socket
    Quick ack mode was activated 372 times
    2656 packets directly queued to recvmsg prequeue.
    9701 bytes directly in process context from backlog
    1625778 bytes directly received in process context from prequeue
    231005 packet headers predicted
    328 packets header predicted and directly queued to user
    286432 acknowledgments not containing data payload received
    1599241 predicted acknowledgments
    37 times recovered from packet loss by selective acknowledgements
    Detected reordering 4 times using FACK
    Detected reordering 6 times using SACK
    Detected reordering 5 times using time stamp
    7 congestion windows fully recovered without slow start
    5 congestion windows partially recovered using Hoe heuristic
    12 congestion windows recovered without slow start by DSACK
    43 congestion windows recovered without slow start after partial ack
    6 timeouts after SACK recovery
    330 fast retransmits
    9 forward retransmits
    266 retransmits in slow start
    119 other TCP timeouts
    TCPLossProbes: 295
    TCPLossProbeRecovery: 8
    389 DSACKs sent for old packets
    19 DSACKs sent for out of order packets
    277 DSACKs received
    531 connections reset due to unexpected data
    776 connections reset due to early user close
    105 connections aborted due to timeout
    TCPDSACKIgnoredNoUndo: 120
    TCPSackShifted: 115
    TCPSackMerged: 121
    TCPSackShiftFallback: 4899
    TCPRcvCoalesce: 48946
    TCPOFOQueue: 18398
    TCPOFOMerge: 18
    TCPChallengeACK: 7
    TCPSYNChallenge: 6
    TCPAutoCorking: 2
    TCPSynRetrans: 36
    TCPOrigDataSent: 3681091
    TCPHystartTrainDetect: 6
    TCPHystartTrainCwnd: 108
    TCPHystartDelayDetect: 1
    TCPHystartDelayCwnd: 34
    TCPACKSkippedPAWS: 2
    TCPACKSkippedSeq: 9
    TCPKeepAlive: 6287
IpExt:
    InMcastPkts: 790
    OutMcastPkts: 1473
    InBcastPkts: 4136
    OutBcastPkts: 3
    InOctets: 1155346657
    OutOctets: 5272261066
    InMcastOctets: 103355
    OutMcastOctets: 290776
    InBcastOctets: 692905
    OutBcastOctets: 234
    InNoECTPkts: 2241771
---------------------------------------------------
Network Tweaks via /etc/sysctl.conf
---------------------------------------------------
#
# /etc/sysctl.conf - Configuration file for setting system variables
# See /etc/sysctl.d/ for additional system variables.
# See sysctl.conf (5) for information.
#

#kernel.domainname = example.com

# Uncomment the following to stop low-level messages on console
#kernel.printk = 3 4 1 3

##############################################################3
# Functions previously found in netbase
#

# Uncomment the next two lines to enable Spoof protection (reverse-path filter)
# Turn on Source Address Verification in all interfaces to
# prevent some spoofing attacks
#net.ipv4.conf.default.rp_filter=1
#net.ipv4.conf.all.rp_filter=1

# Uncomment the next line to enable TCP/IP SYN cookies
# See http://lwn.net/Articles/277146/
# Note: This may impact IPv6 TCP sessions too
#net.ipv4.tcp_syncookies=1

# Uncomment the next line to enable packet forwarding for IPv4
#net.ipv4.ip_forward=1

# Uncomment the next line to enable packet forwarding for IPv6
#  Enabling this option disables Stateless Address Autoconfiguration
#  based on Router Advertisements for this host
#net.ipv6.conf.all.forwarding=1


###################################################################
# Additional settings - these settings can improve the network
# security of the host and prevent against some network attacks
# including spoofing attacks and man in the middle attacks through
# redirection. Some network environments, however, require that these
# settings are disabled so review and enable them as needed.
#
# Do not accept ICMP redirects (prevent MITM attacks)
#net.ipv4.conf.all.accept_redirects = 0
#net.ipv6.conf.all.accept_redirects = 0
# _or_
# Accept ICMP redirects only for gateways listed in our default
# gateway list (enabled by default)
# net.ipv4.conf.all.secure_redirects = 1
#
# Do not send ICMP redirects (we are not a router)
#net.ipv4.conf.all.send_redirects = 0
#
# Do not accept IP source route packets (we are not a router)
#net.ipv4.conf.all.accept_source_route = 0
#net.ipv6.conf.all.accept_source_route = 0
#
# Log Martian Packets
#net.ipv4.conf.all.log_martians = 1
#
